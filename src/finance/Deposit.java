package finance;

/**
 * Created by Максим on 24.03.2017.
 */
public interface Deposit {
    double getDeposit(int uah);

    boolean couldGetDeposit(int timeDeposit);
}
