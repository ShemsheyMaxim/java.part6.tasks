package finance;

import finance.Bank;
import finance.BlackMarket;
import finance.Finance;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Максим on 24.03.2017.
 */
public class Main {
    public static void main(String[] args) {

        ArrayList <Finance> finances = new ArrayList<Finance>();
        finances.add(new Bank("ПриватБанк", "ул. Набережная Победы, 50, г.Днепр,", 2009, 27.3, 29.45, 0.25, 200_000, 0.15, 0.01, 5));
        finances.add(new Bank("ОщадБанк", "ул. Госпитальная, 12-Г, г.Киев,", 1991, 27.28, 29.45, 0.22, 200_000, 0.16, 0.01, 5));
        finances.add(new BlackMarket("ЮЖД", "г.Харьков", 28.2, 30));
        finances.add(new CreditCafe("Подкрепись деньгами", "ул. Московський проспект, 144, Харків", 2, 4_000));
        finances.add(new CreditUnion("Семейный Кредит", "вулиця Римарська, 20, Харків", 0.2, 100_000));
        finances.add(new MIF("Sun Group", "г. Харьков, ул. Киргизская, 19-Б", 1999, 0.14));
        finances.add(new Pawnshop("НАДІЯ", "ул. Данилевского, 18, г.Харьков", 0.4, 50_000));
        finances.add(new Post("Укрпочта", "ул. Кирова, 6, г.Харьков", 0.02));

        Collections.sort(finances);
        for (Finance finance : finances){
            System.out.println("name: " + finance.getName());
        }

        int cashForExchange = 20_000;
        Finance bestExchangeForUSD = null;
        Finance bestExchangeForEUR = null;
        double bestCoursesForExchangeEUR = -1;
        double bestCoursesForExchangeUSD = -1;

        for (Finance finance : finances) {
            if (finance instanceof Exchange) {
                Exchange exchangeFinance = (Exchange) finance;
                if (((Exchange) finance).couldExchangeAmountUAH(cashForExchange)) {
                    double exchangeResultInUSD = exchangeFinance.convertUAHinUSD(cashForExchange);
                    double exchangeResultinEUR = exchangeFinance.convertUAHinEUR(cashForExchange);
                    if (exchangeResultInUSD > bestCoursesForExchangeUSD) {
                        bestCoursesForExchangeUSD = exchangeResultInUSD;
                        bestExchangeForUSD = finance;
                    }
                    if (exchangeResultinEUR > bestCoursesForExchangeEUR) {
                        bestCoursesForExchangeEUR = exchangeResultinEUR;
                        bestExchangeForEUR = finance;

                    }
                }
            }
        }
        System.out.println("USD: " + bestCoursesForExchangeUSD);
        System.out.println("EUR: " + bestCoursesForExchangeEUR);
        System.out.println("The best exchange for EUR: " + bestExchangeForEUR.getInfo());
        System.out.println("The best exchange for USD: " + bestExchangeForUSD.getInfo());

        int amountOfCredit = 50_000;
        Finance bestOrganizationForCredit = null;
        double bestInterestRateForCredit = Double.MAX_VALUE;

        for (Finance finance : finances) {
            if (finance instanceof Credit) {
                Credit creditFinance = (Credit) finance;
                if (((Credit) finance).couldGiveCredit(amountOfCredit)) {
                    double creditFinanceResult = creditFinance.getCredit(amountOfCredit);
                    if (bestInterestRateForCredit > creditFinanceResult) {
                        bestInterestRateForCredit = creditFinanceResult;
                        bestOrganizationForCredit = finance;
                    }
                }
            }
        }
        System.out.println("You will need return this amount of money if you take credit: " + bestInterestRateForCredit + " UAH.");
        System.out.println("The best organization for credit: " + bestOrganizationForCredit.getInfo());

        int cashForDeposit = 10_000;
        int timeDepisit = 12;
        Finance bestOrganizationForDeposit = null;
        double bestInterestRateForDeposit = -1;

        for (Finance finance : finances) {
            if (finance instanceof Deposit) {
                Deposit depositFinance = (Deposit) finance;
                if (((Deposit) finance).couldGetDeposit(timeDepisit)) {
                    double depositFinanceResult = depositFinance.getDeposit(cashForDeposit);
                    if (bestInterestRateForDeposit < depositFinanceResult) {
                        bestInterestRateForDeposit = depositFinanceResult;
                        bestOrganizationForDeposit = finance;
                    }
                }

            }
        }
        System.out.println("You will return this amount of money if you put money on deposit: " + bestInterestRateForDeposit + " UAH.");
        System.out.println("The best organization for deposit: " + bestOrganizationForDeposit.getInfo());

        int cashForRemittance = 1_000;
        Finance bestOrganizedForRemittance = null;
        double bestInterestRateForRemittance = -1;

        for (Finance finance : finances) {
            if (finance instanceof Remittance) {
                Remittance remittanceFinance = (Remittance) finance;
                double remittanceFinanceResult = remittanceFinance.getRemittance(cashForRemittance);
                if (bestInterestRateForRemittance < remittanceFinanceResult) {
                    bestInterestRateForRemittance = remittanceFinanceResult;
                    bestOrganizedForRemittance = finance;
                }
            }
        }
        System.out.println("You can remittance: " + bestInterestRateForRemittance + " UAH.");
        System.out.println("The best organization for remittance: " + bestOrganizedForRemittance.getInfo());
    }
}
