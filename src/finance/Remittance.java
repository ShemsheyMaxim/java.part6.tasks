package finance;

/**
 * Created by Максим on 24.03.2017.
 */
public interface Remittance {
    double getRemittance(double uah);
}
